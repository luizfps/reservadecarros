package testelemaf;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ScrollPaneLayout;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import org.jdatepicker.JDatePicker;

public class Main {

	public static void main(String[] args) throws IOException {
		
		LeitorArquivoLojas leitorLojas = new LeitorArquivoLojas("ListaLojas");
		ArrayList<Lojas>lojas = leitorLojas.lerLojas();
		
		LeitorArquivoCarros leitorCarros = new LeitorArquivoCarros("ListaCarros");
		ArrayList<Carros>carros = leitorCarros.carregarCarros(lojas);
						
		ArrayList<JDatePicker>datas = new ArrayList<JDatePicker>();
		
		ArrayList<String>Entradas = new ArrayList<String>();
		
		
		
			
		
		// TODO Auto-generated method stub
		
		
		JPanel panel = new JPanel();
		panel.setBorder(javax.swing.BorderFactory.createTitledBorder("Dados de entrada"));
		panel.setLayout(new FlowLayout(FlowLayout.LEADING));
		panel.setPreferredSize(new Dimension(600,50));
		
		JTextField campoNumeroPassageiros;
		campoNumeroPassageiros = new JTextField(5);
	
		
		JLabel labelNPassageiros = new JLabel();
		labelNPassageiros.setText("Numeros de lugares desejado:");
		labelNPassageiros.setToolTipText("deve ser digitado um n�mero inteiro!");
		panel.add(labelNPassageiros);
		panel.add(campoNumeroPassageiros);
				
		JPanel panelDatas = new JPanel();
		panelDatas.setLayout(new BoxLayout(panelDatas, BoxLayout.Y_AXIS));
	
		JDatePicker jdpicker = new JDatePicker();
		jdpicker.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		panelDatas.add(jdpicker);
		datas.add(jdpicker);
		
		JButton botao = new JButton("adicionar");
		botao.setPreferredSize(new Dimension(100,140));
		botao.setToolTipText("adiconar mais datas");
		
		JScrollPane panel2 = new JScrollPane(panelDatas);
		panel2.setLayout(new ScrollPaneLayout());
		panel2.setBorder(javax.swing.BorderFactory.createTitledBorder("datas"));
		panel2.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		panel2.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		panel2.setPreferredSize(new Dimension(480,150));
		
		botao.addActionListener( new ActionListener() { 
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JDatePicker jdpicker = new JDatePicker();
				jdpicker.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
				datas.add(jdpicker);
				panelDatas.add(jdpicker);
				panel2.revalidate();
				
			}});
			
				
		JPanel panel3 = new JPanel();
		panel3.add(botao);

		JScrollPane panel4 = new JScrollPane();
		panel4.setLayout(new ScrollPaneLayout());
		panel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Carros e custos"));
		panel4.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		panel4.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		panel4.setPreferredSize(new Dimension(600,200));
		
		JButton botaoPesquisar = new JButton("pesquisar");
		botaoPesquisar.setPreferredSize(new Dimension(600,50));
		JPanel panel5 = new JPanel();
		panel5.add(botaoPesquisar);
				
		botaoPesquisar.addActionListener( new ActionListener() { 
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				Boolean tudoPreenchido = true;
					
				LojasCarrosGerenciador gerenciador = new LojasCarrosGerenciador(carros);
				
				Entradas.add(campoNumeroPassageiros.getText().toString());
				
				for (JDatePicker datapicker : datas) {
					if(datapicker.getDateFormat()==null) {
						tudoPreenchido = false;
					}
					System.out.print(datapicker.getDateFormat());
				}
				System.out.print(Entradas.get(0).toString());
				if(Entradas.get(0).equals("")&& tudoPreenchido==false) {
					JOptionPane.showMessageDialog(null,
					        "Preencha todos os dados!", //mensagem
					        "Dados invalidos", // titulo da janela 
					        JOptionPane.INFORMATION_MESSAGE);
				}else {
					
					ArrayList<Carros>carrosSelecionadosAux = gerenciador.processarDados(Entradas.toArray(new String[Entradas.size()]));
					
					String[] headerTabela = {"loja Proprietaria","tipo Carro","modelo","numero Passageiros","custo Aluguel"};
					
					Object [][]obj = new String[carrosSelecionadosAux.size()][5];
							
					PreencherTabela(obj,carrosSelecionadosAux);
					
					JTable panelCarros = new JTable( obj,headerTabela);
					panelCarros.getInsets().set(10, 10, 10, 10);
					
					panel4.setViewportView(panelCarros);
				}
				
			}});
							
		
		
		JFrame janela = new JFrame("Prova lemaf");
		GridBagLayout gblayout = new GridBagLayout();
		janela.setLayout(gblayout);
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth=2;
		gbc.weightx = 0.1;
		gbc.insets = new Insets(10,10,1,10); 
		gbc.fill = GridBagConstraints.HORIZONTAL;
		janela.add(panel,gbc);
		
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.gridwidth=1;
		gbc.insets = new Insets(0,10,1,10);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		janela.add(panel2,gbc);
		
		gbc.gridx = 1;
		gbc.gridy = 1;
		gbc.insets = new Insets(10,10,1,10);
		gbc.fill = GridBagConstraints.HORIZONTAL;
		janela.add(panel3,gbc);
		
		gbc.insets = new Insets(0,10,1,10); 
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridwidth=2;
		gbc.gridx = 0;
		gbc.gridy = 2;
		janela.add(panel5,gbc);
		
		gbc.gridwidth=3;
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.weightx = 0.1;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.insets = new Insets(0,10,10,10); 
		janela.add(panel4,gbc);
		
		janela.setResizable(false);
		janela.pack();
		janela.setBackground(Color.gray);
		janela.setVisible(true);
		janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		

		
		
	}
	public static void PreencherTabela(Object [][]obj,ArrayList<Carros>carrosSelecionados) {
		
		
		
		
		for(int i=0;i<carrosSelecionados.size();i++) {
			obj[i][0]=carrosSelecionados.get(i).getLojaProprietaria().getNome().toString();
			obj[i][1]=carrosSelecionados.get(i).getTipoCarro().toString();
			obj[i][2]=carrosSelecionados.get(i).getModelo().toString();
			obj[i][3]=Integer.toString(carrosSelecionados.get(i).getNumeroPassageiros());
			obj[i][4]=Float.toString(carrosSelecionados.get(i).getCustoDeAluguel());

		}
		
	}
	
}
