package testelemaf;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class LeitorArquivoLojas extends LeitorArquivos {
	

	public LeitorArquivoLojas(String pathArquivo) throws FileNotFoundException {
		super(pathArquivo);
		// TODO Auto-generated constructor stub
	}
	
	public ArrayList<Lojas> lerLojas() throws IOException{
		
		ArrayList<Lojas> lojas =new ArrayList<>();
		
		Lojas lojaLida;
		
		while(br.ready()){ 
			
	   		String linha = br.readLine(); 
	   		String[] parser = linha.split(",");
	   		
	   		lojaLida = new Lojas(parser[0],Float.parseFloat(parser[1]),Float.parseFloat(parser[2]),Float.parseFloat(parser[3]),Float.parseFloat(parser[4]));
	   	
	   		lojas.add(lojaLida);
		
		} 
			br.close(); 
			
			
			
		return lojas;
		
	}

}
