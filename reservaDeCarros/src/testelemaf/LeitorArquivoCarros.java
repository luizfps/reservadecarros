package testelemaf;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class LeitorArquivoCarros extends LeitorArquivos {

	public LeitorArquivoCarros(String pathArquivo) throws FileNotFoundException {
		super(pathArquivo);
		// TODO Auto-generated constructor stub
	}
	
public ArrayList<Carros> carregarCarros(ArrayList<Lojas>lojas) throws IOException{
		
		ArrayList<Carros> listaCarros =new ArrayList<>();
		ArrayList<Lojas>lojasProprietaria = lojas;
		
		Carros carros = null;
		
		while(br.ready()){ 
			
	   		String linha = br.readLine(); 
	   		String[] parser = linha.split(",");
	   		
	   		for(int i=0;i<lojas.size();i++) {
	   			if(lojas.get(i).getNome().equals(parser[0])) {
	   				carros = new Carros(lojas.get(i),parser[1],parser[3],Integer.parseInt(parser[2]));
	   			}
	   		}
	   		
	   		listaCarros.add(carros);
		
		} 
			br.close(); 
			
			
			
		return listaCarros;
		
	}
}
