package testelemaf;

public class Carros {
	
	private Lojas lojaProprietaria;
	private String tipoCarro;
	private String modelo;
	private boolean disponibilidade;
	private int numeroPassageiros;
	private float custoDeAluguel;
	
	public Carros() {
		this.custoDeAluguel = 0;
	}
	
		

	public Carros(Lojas lojaProprietaria, String tipoCarro, String modelo, int numeroPassageiros) {
		super();
		this.lojaProprietaria = lojaProprietaria;
		this.tipoCarro = tipoCarro;
		this.modelo = modelo;
		this.numeroPassageiros = numeroPassageiros;
		this.custoDeAluguel = 0;
	}



	public Lojas getLojaProprietaria() {
		return lojaProprietaria;
	}
	
	public void setLojaProprietaria(Lojas lojaProprietaria) {
		this.lojaProprietaria = lojaProprietaria;
	}
	
	public String getTipoCarro() {
		return tipoCarro;
	}
	
	public void setTipoCarro(String tipoCarro) {
		this.tipoCarro = tipoCarro;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public int getNumeroPassageiros() {
		return numeroPassageiros;
	}

	public void setNumeroPassageiros(int numeroPassageiros) {
		this.numeroPassageiros = numeroPassageiros;
	}

	public boolean isDisponibilidade() {
		return disponibilidade;
	}

	public void setDisponibilidade(boolean disponibilidade) {
		this.disponibilidade = disponibilidade;
	}

	public float getCustoDeAluguel() {
		return custoDeAluguel;
	}

	public void setCustoDeAluguel(float custoDeAluguel) {
		this.custoDeAluguel = custoDeAluguel;
	}
	
	public void calcularCusto(float custo) {
		this.custoDeAluguel += custo;
		
	}
	

	
	
	@Override
	public String toString() {
		return "[lojaProprietaria=" + lojaProprietaria.getNome() + ", tipoCarro=" + tipoCarro + ", modelo=" + modelo
				+ " numeroPassageiros=" + numeroPassageiros
				+ ", custoDeAluguel=" + custoDeAluguel + "]";
	}

	
}
