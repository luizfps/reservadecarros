package testelemaf;

import java.io.FileNotFoundException;
import java.io.IOException;

public class LeitorEntrada extends LeitorArquivos {

	public LeitorEntrada(String pathArquivo) throws FileNotFoundException {
		super(pathArquivo);
		// TODO Auto-generated constructor stub
	}
	public String[] lerEntrada() throws IOException {
		
		String[] parser = null;
		
		while(br.ready()){ 

			String linha = br.readLine(); 
			parser = linha.split("[,:]");
		} 
		br.close(); 

		return parser;
	}

}
