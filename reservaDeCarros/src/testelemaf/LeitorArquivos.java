package testelemaf;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class LeitorArquivos {
	
	protected BufferedReader br;
	private String pathArquivo;
	
	
	public LeitorArquivos(String pathArquivo ) throws FileNotFoundException {
		
		this.pathArquivo = pathArquivo;
		br = new BufferedReader(new FileReader(pathArquivo));
	}
		
}
