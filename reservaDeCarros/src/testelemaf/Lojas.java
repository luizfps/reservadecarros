/**
 * 
 */
package testelemaf;

public class Lojas {

	private String nome;
	private float taxaFimsemanaClienteRegular;
	private float taxaFimsemanaClienteFidelidade;
	private float taxaSemanaClienteRegular;
	private float taxaSemanaClienteFidelidade;
	
	
	
	public Lojas(String nome,float taxaSemanaClienteRegular, float taxaSemanaClienteFidelidade,float taxaFimsemanaClienteRegular, float taxaFimsemanaClienteFidelidade
			 ) {
		
		this.nome = nome;
		this.taxaFimsemanaClienteRegular = taxaFimsemanaClienteRegular;
		this.taxaFimsemanaClienteFidelidade = taxaFimsemanaClienteFidelidade;
		this.taxaSemanaClienteRegular = taxaSemanaClienteRegular;
		this.taxaSemanaClienteFidelidade = taxaSemanaClienteFidelidade;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public float getTaxaFimsemanaClienteRegular() {
		return taxaFimsemanaClienteRegular;
	}
	public void setTaxaFimsemanaClienteRegular(float taxaFimsemanaClienteRegular) {
		this.taxaFimsemanaClienteRegular = taxaFimsemanaClienteRegular;
	}
	public float getTaxaFimsemanaClienteFidelidade() {
		return taxaFimsemanaClienteFidelidade;
	}
	public void setTaxaFimsemanaClienteFidelidade(float taxaFimsemanaClienteFidelidade) {
		this.taxaFimsemanaClienteFidelidade = taxaFimsemanaClienteFidelidade;
	}
	public float getTaxaSemanaClienteRegular() {
		return taxaSemanaClienteRegular;
	}
	public void setTaxaSemanaClienteRegular(float taxaSemanaClienteRegular) {
		this.taxaSemanaClienteRegular = taxaSemanaClienteRegular;
	}
	public float getTaxaSemanaClienteFidelidade() {
		return taxaSemanaClienteFidelidade;
	}
	public void setTaxaSemanaClienteFidelidade(float taxaSemanaClienteFidelidade) {
		this.taxaSemanaClienteFidelidade = taxaSemanaClienteFidelidade;
	}

	@Override
	public String toString() {
		return "Lojas [nome=" + getNome() + ", taxaFimsemanaClienteRegular=" + taxaFimsemanaClienteRegular
				+ ", taxaFimsemanaClienteFidelidade=" + taxaFimsemanaClienteFidelidade + ", taxaSemanaClienteRegular="
				+ taxaSemanaClienteRegular + ", taxaSemanaClienteFidelidade=" + taxaSemanaClienteFidelidade + "]";
	}
	
}
