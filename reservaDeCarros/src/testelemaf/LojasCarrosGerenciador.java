package testelemaf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class LojasCarrosGerenciador {
	
	private ArrayList<Carros> carros;
	
	public LojasCarrosGerenciador(ArrayList<Carros> carros) {
		
		this.carros = carros;
				
	}
	
	private void escolherCarroPorTipo(String tipoCarro) {
		
		ArrayList<Carros>novaLista = new ArrayList();
		
		for(int i=0;i<carros.size();i++) {
			if(carros.get(i).getTipoCarro().equals(tipoCarro)) {
				novaLista.add(carros.get(i));
			}
		}
		if(!novaLista.isEmpty()) {
			carros = novaLista;
		}
	}
	
	private void escolherCarroPorPassageiros(int nPassageiros) {
		
		ArrayList<Carros>novaLista = new ArrayList();
				
		for(int i=0;i<carros.size();i++) {
			if(carros.get(i).getNumeroPassageiros()>=nPassageiros) {
				novaLista.add(carros.get(i));
			}
		}
		if(!novaLista.isEmpty()) {
			carros = novaLista;
		}
	}
	
	public void calcularCustoPorData(String[] datas) {

		for(int j=0;j<carros.size();j++) {
			for (int i=0;i<datas.length;i++) {
				if(datas[i].contains("sab")||datas[i].contains("dom")) {
					carros.get(j).calcularCusto(carros.get(j).getLojaProprietaria().getTaxaFimsemanaClienteRegular());
				}
				else {
					carros.get(j).calcularCusto(carros.get(j).getLojaProprietaria().getTaxaSemanaClienteRegular());
				}
			}
		}
		
	}
	
	private void selecaoMelhorCustoBeneficio() {
		
		
		Collections.sort(carros, new OrdenacaoPorCusto());
		
		for (Carros carros2 : carros) {
			carros2.toString();
		}
	}
	
	public ArrayList<Carros> processarDados(String[]dados) {
		
		//String tipoCarro = dados[0];
		int numLugares = Integer.parseInt(dados[0]);
		String[] diasSemana = new String[dados.length-1];
		
		for(int i=1;i<dados.length;i++) {
			diasSemana[i-1] = dados[i];
		}
		
		
		//escolherCarroPorTipo(tipoCarro);
		escolherCarroPorPassageiros(numLugares);
		calcularCustoPorData(diasSemana);
		selecaoMelhorCustoBeneficio();
		
		return carros;
	}
	
	
	class OrdenacaoPorCusto implements Comparator<Carros> 
	{ 
	    
	    public int compare(Carros a, Carros b) 
	    { 
	        return (int)a.getCustoDeAluguel() - (int)b.getCustoDeAluguel(); 
	    } 
	} 

}
